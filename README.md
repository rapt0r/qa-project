Тестирование сайта https://www.avtodispetcher.ru/

Инициализация проекта:
```bash
1. Создаем виртуальное окружение 
2. pip install -U pip 
3. pip install -r requirements.txt

```
Запуск тестов:
```bash
pytest tests/test_main.py
```

Создание allure отчета:
```bash
pytest --alluredir allure_results
```

Открытие allure отчета:
```shell script
allure serve allure_results
```
test2 - new branch