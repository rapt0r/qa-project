from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

from locators import Main, YandexSearchResults, YandexSearch
import allure
import time


@allure.title("Тестирование сайта avtodispetcher.ru")
@allure.severity(severity_level='blocker')
def test_avtodispetcher(browser):

    wait = WebDriverWait(browser, 10)
    with allure.step('Заходим на https://yandex.ru/, вбиваем в поиск "Расчет расстояний между городами" и запускаем поиск'):
        input_search = browser.find_element_by_css_selector(YandexSearch.search.input['css'])
        input_search.send_keys(Main.variables.title)
        browser.find_element_by_css_selector(YandexSearch.search.button['css']).click()

    with allure.step('Ищем среди результатов avtodispetcher.ru и переходим по ссылке'):
        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, 'li.serp-item')))
        list_elements = browser.find_elements_by_css_selector(YandexSearchResults.search_results.list['css'])
        for list_element in list_elements:
            b = list_element.find_element_by_css_selector(YandexSearchResults.search_results.text_link['css'])
            if b.text == 'avtodispetcher.ru':
                list_element.find_element_by_css_selector(YandexSearchResults.search_results.link['css']).click()
                wait.until(ec.visibility_of_all_elements_located((By.TAG_NAME, 'html')))
        time.sleep(2)

    with allure.step('Проверяем title страницы и url'):
        new_window = browser.window_handles[1]
        browser.switch_to.window(new_window)
        current_title = browser.title
        current_url = browser.current_url
        assert current_title == Main.variables.title
        assert current_url == Main.variables.url

    with allure.step('Рассчитываем маршрут. Проверяем расстояние и стоимость топлива'):
        input_from = browser.find_element_by_css_selector(Main.featured.field_from['css'])
        input_to = browser.find_element_by_css_selector(Main.featured.field_to['css'])
        fuel_consumption = browser.find_element_by_css_selector(Main.featured.field_fuel_consumption['css'])
        fuel_price = browser.find_element_by_css_selector(Main.featured.field_fuel_price['css'])
        button_calculate = browser.find_element_by_xpath(Main.featured.button_calculate['xpath'])

        input_from.clear()
        input_to.clear()
        fuel_consumption.clear()
        fuel_price.clear()

        input_from.send_keys(Main.variables.city_from)
        input_to.send_keys(Main.variables.city_to)
        fuel_consumption.send_keys(Main.variables.consumption1)
        fuel_price.send_keys(Main.variables.price1)
        browser.execute_script("window.scrollTo(0, 100)")
        button_calculate.click()

        total_distance = browser.find_element_by_css_selector(Main.featured.total_distance['css'])
        fuel_cost = browser.find_element_by_css_selector(Main.featured.fuel_cost['css'])
        assert total_distance.text == Main.variables.distance1
        assert fuel_cost.text[-9:-5] == Main.variables.coust1

    with allure.step('Изменяем маршрут. Снова проверяем расстояние и стоимость топлива'):
        time.sleep(1)

        configure_route = browser.find_element_by_css_selector(Main.routeChange.configure_route['css'])
        configure_route.click()
        browser.execute_script("window.scrollTo(0, 400)")

        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, 'label#inter_points_field_parent > input')))

        cross_town = browser.find_element_by_css_selector(Main.routeChange.cross_town['css'])
        cross_town.send_keys(Main.variables.city_change)
        time.sleep(1)

        browser.execute_script("window.scrollTo(0, 600)")
        button_calculate = browser.find_element_by_xpath(Main.featured.button_calculate['xpath'])
        button_calculate.click()

        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, 'span#totalDistance')))

        total_distance = browser.find_element_by_css_selector(Main.featured.total_distance['css'])
        fuel_cost = browser.find_element_by_css_selector(Main.featured.fuel_cost['css'])
        assert total_distance.text == Main.variables.distance2
        assert fuel_cost.text[-9:-5] == Main.variables.coust2


# def test1(browser2):
#     browser2.get('https://google.com')
#     elem = browser2.find_element_by_name('q')
#     elem.click()