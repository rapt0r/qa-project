import pytest
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption('--browser', action='store', default='chrome', help='write your browser')
    parser.addoption('--url', action='store', default='https://yandex.ru', help='write your url')

@pytest.fixture
def browser(request):
    browser = request.config.getoption('--browser')
    url = request.config.getoption('--url')
    if browser == 'chrome':
        driver = webdriver.Chrome()
    elif browser == 'firefox':
        driver = webdriver.Firefox()
    elif browser == 'opera':
        driver = webdriver.Opera()
    else:
        raise Exception(f"{request.param} is not supported!")

    driver.implicitly_wait(15)
    driver.get(url)
    return driver
# @pytest.fixture
# def browser(request):
#     url = request.config.getoption('--url')
#     driver = webdriver.Chrome()
#     driver.implicitly_wait(15)
#     driver.get(url)
#     return driver
#
# @pytest.fixture
# def browser2(request):
#     url = request.config.getoption('--url')
#     driver = webdriver.Firefox()
#     driver.implicitly_wait(15)
#     driver.get(url)
#     return driver
#
# @pytest.fixture
# def browser3(request):
#     url = request.config.getoption('--url')
#     driver = webdriver.Opera()
#     driver.implicitly_wait(15)
#     driver.get(url)
#     return driver

# BROWSERSTACK_URL = 'https://eder45:AbJmThoFRV2yb5xx2hKh@hub-cloud.browserstack.com/wd/hub'
#
# desired_cap = {
#
#     'os' : 'Windows',
#     'os_version' : '10',
#     'browser' : 'Chrome',
#     'browser_version' : '80',
#     'name' : "eder45's First Test"
#
# }

# @pytest.fixture
# def browser(request):
#     driver = webdriver.Remote(command_executor=BROWSERSTACK_URL, desired_capabilities=desired_cap)
#     # driver = webdriver.Remote("http://localhost:4444/wd/hub",
#     #                           desired_capabilities={'browserName': 'chrome', 'version': '', 'platform': 'ANY'})
#     driver.get('https://yandex.ru')
#     request.addfinalizer(driver.quit)
#     return driver

# def test1(browser):
#     browser.get('https://google.com')
#     elem = browser.find_element_by_name('q')
#     elem.click()