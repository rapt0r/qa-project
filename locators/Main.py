class Main:

    class variables:
        title = 'Расчет расстояний между городами'
        url = 'https://www.avtodispetcher.ru/distance/'
        city_from = 'Тула'
        city_to = 'Санкт-Петербург'
        city_change = 'Великий Новгород'
        consumption1 = '9'
        price1 = '46'
        distance1 = '897'
        coust1 = '3726'
        distance2 = '966'
        coust2 = '4002'

    class featured:
        field_from = {'css': 'input[name="from"]'}
        field_to = {'css': 'input[name="to"]'}
        field_fuel_consumption = {'css': 'input[name="fc"]'}
        field_fuel_price = {'css': 'input[name="fp"]'}
        button_calculate = {'xpath': '//input[@value="Рассчитать"]'}
        total_distance = {'css': 'span#totalDistance'}
        fuel_cost = {'css': 'form > p'}

    class routeChange:
        configure_route = {'css': 'a#triggerFormD'}
        cross_town = {'css': 'label#inter_points_field_parent > input'}
