class YandexSearchResults:

    class search_results:
        link = {'css': 'a.link'}
        list = {'css': 'li.serp-item'}
        text_link = {'css': 'a.link > b'}